<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Barang extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_barang');
		$this->load->library('form_validation');
	}

	function index()
	{
		belum_login();
		$data['data'] = $this->M_barang->tampil_data();

		$this->load->view('barang/v_barang', $data);
	}

	function inven()
	{
		belum_login();
		$data['data'] = $this->M_barang->tampil_data();

		$this->load->view('barang/inventaris_barang', $data);
	}

	function inventaris()
	{
		cek_belum_login();
		$data['data'] = $this->M_barang->tampil_data();

		$this->load->view('barang/inventaris', $data);
	}

	function add()
	{
		belum_login();
		$this->form_validation->set_rules('kode_barang','Kode Barang','required|is_unique[barang.kode_barang]');
		$this->form_validation->set_rules('nama_barang','Nama Barang','required');
		$this->form_validation->set_rules('kondisi_barang','Kondisi Barang','required');
		$this->form_validation->set_rules('keterangan_barang','Keterangan Barang','required');
		$this->form_validation->set_rules('status','Status','required');

		// memberikan pesan jika input yg wajib di isi tidak di isi
		$this->form_validation->set_message('required', '%s masih kosong, silahkan isi');

		// menampilkan pesan jika nik sudah tersedia
		$this->form_validation->set_message('is_unique','{field} sudah terdaftar');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('barang/tambah_barang');
		} else {
			$post = $this->input->post(null, TRUE);
			$this->M_barang->add_barang($post);
			if ($this->db->affected_rows() > 0) {
				echo "<script>alert('Data berhasil disimpan');</script>";
			}
			echo "<script>window.location='".base_url('barang')."';</script>";
		}
	}

	function edit($kode_barang='')
	{
		belum_login();
		$this->form_validation->set_rules('kode_barang','Kode Barang','required');
		$this->form_validation->set_rules('nama_barang','Nama Barang','required');
		$this->form_validation->set_rules('kondisi_barang','Kondisi Barang','required');
		$this->form_validation->set_rules('keterangan_barang','Keterangan Barang','required');
		$this->form_validation->set_rules('status','Status','required');

		// memberikan pesan jika input yg wajib di isi tidak di isi
		$this->form_validation->set_message('required', '%s masih kosong, silahkan isi');

		if ($this->form_validation->run() == FALSE) {
			$query = $this->M_barang->tampil_data($kode_barang);
			if ($query->num_rows() > 0) {
				$data['data'] = $query->row();
				$this->load->view('barang/edit_barang',$data);
			}
		} else {
			$post = $this->input->post(null, TRUE);
			$this->M_barang->edit_barang($post);
			if ($this->db->affected_rows() > 0) {
				echo "<script>alert('Data berhasil diedit');</script>";
			}
			echo "<script>window.location='".base_url('barang')."';</script>";
		}
	}

	function hapus()
	{
		$kode_barang = $this->input->post('kode_barang');
		$this->M_barang->del($kode_barang);

		if ($this->db->affected_rows() > 0) {
			echo "<script>alert('Data berhasil dihapus');</script>";
		}
		echo "<script>window.location='".site_url('barang')."';</script>";
	}

	
}