<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Auth extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_pegawai');
		$this->load->model('M_user','',TRUE);
	}

	function index()
	{
		$this->load->view('pilih');
	}

	function login()
	{
		// proses login
		$post = $this->input->post(null, TRUE);
		if (isset($post['login'])) {
			$query = $this->M_pegawai->cekAkunP($post);
			if ($query->num_rows() > 0) {
				$row = $query->row();
				$params = array(
					'nik' => $row->nik,
					'level' => $row->level
				);
				// menampilkan pemberitahuan jika berhasil login
				$this->session->set_userdata($params);
				echo "<script>
				alert('Login Berhasil');
				window.location='".base_url('dashboard/p_dashboard')."';
				</script>";
			} else {
				// menampilkan pemberitahuan jika login gagal
				echo "<script>
				alert('Login Gagal, Username atau Password salah!');
				window.location='".base_url('auth/login')."';
				</script>";
			}
		}

		// view login pegawai
		$this->load->view('auth/login');
	}

	function logout()
	{
		belum_login();
		$params = array('nik','level');
		$this->session->unset_userdata($params);
		redirect(base_url());
	}

	// proses register akun
	function register()
	{
		// proses menambahkan user pegawai
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nik','NIK','required|is_unique[pegawai.nik]');
		$this->form_validation->set_rules('nama_pegawai','Nama Pegawai','required');
		$this->form_validation->set_rules('password','Password','required|min_length[5]');
		$this->form_validation->set_rules('passconf','Ulangi Password','required|matches[password]', array('matches' => '%s tidak sama'));
		$this->form_validation->set_rules('telepon','Telepon','required');
		$this->form_validation->set_rules('level','Level','required');
		$this->form_validation->set_rules('status','Status','required');

		// memberikan pesan jika input yg wajib di isi tidak di isi
		$this->form_validation->set_message('required', '%s masih kosong, silahkan isi');

		// menampilkan pesan jika nik sudah tersedia
		$this->form_validation->set_message('is_unique','{field} sudah terdaftar');

		// menampilkan pesan jika password kurang dari 5
		$this->form_validation->set_message('min_length', '{field} minimal  karakter');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('auth/register');
		} else {
			$post = $this->input->post(null, TRUE);
			$this->M_pegawai->regis($post);
			if ($this->db->affected_rows() > 0) {
				echo "<script>alert('Data berhasil disimpan');</script>";
			}
			echo "<script>window.location='".base_url('auth/register')."';</script>";
		}
	}

	function v_user()
	{
		belum_login();
		// load model dari M_user untuk menampilkan data function get()
		$data['row'] = $this->M_user->tampil();

		$this->load->view('auth/tbl_user', $data);
	}

	function hapus_user()
	{
		$nipd = $this->input->post('nipd');
		$this->M_user->del($nipd);

		if ($this->db->affected_rows() > 0) {
			echo "<script>alert('Data berhasil dihapus');</script>";
		}
		echo "<script>window.location='".site_url('auth/v_user')."';</script>";
	}

}