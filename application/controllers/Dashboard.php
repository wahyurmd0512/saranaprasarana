<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Dashboard extends CI_Controller
{
	function p_dashboard()
	{
		// cek jika belum login
		belum_login();

		$this->load->view('auth/dashboard');
	}

	function u_dashboard()
	{
		// cek jika belum login
		cek_belum_login();

		$this->load->view('user/dashboard');
	}
}