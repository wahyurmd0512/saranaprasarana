<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class User extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_user');
	}

	function login()
	{
		// proses login
		$post = $this->input->post(null, TRUE);
		if (isset($post['login'])) {
			$query = $this->M_user->cekAkunU($post);
			if ($query->num_rows() > 0) {
				$row = $query->row();
				$params = array(
					'nipd' => $row->nipd,
					'walas' => $row->walas
				);
				// menampilkan pemberitahuan jika berhasil login
				$this->session->set_userdata($params);
				echo "<script>
				alert('Login Berhasil');
				window.location='".base_url('dashboard/u_dashboard')."';
				</script>";
			} else {
				// menampilkan pemberitahuan jika login gagal
				echo "<script>
				alert('Login Gagal, Username atau Password salah!');
				window.location='".base_url('user/login')."';
				</script>";
			}
		}

		// menampilkan view login user
		$this->load->view('user/login');
	}

	function logout()
	{
		$params = array('nipd','walas');
		$this->session->unset_userdata($params);
		redirect(base_url());
	}

	function register()
	{
		// proses menambahkan user pegawai
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nipd','NIPD','required|is_unique[peminjam.nipd]');
		$this->form_validation->set_rules('nama_peminjam','Nama Peminjam','required');
		$this->form_validation->set_rules('password','Password','required|min_length[5]');
		$this->form_validation->set_rules('passconf','Ulangi Password','required|matches[password]', array('matches' => '%s tidak sama'));
		$this->form_validation->set_rules('telepon','Telepon','required');
		$this->form_validation->set_rules('walas','walas','required');
		$this->form_validation->set_rules('status','Status','required');

		// memberikan pesan jika input yg wajib di isi tidak di isi
		$this->form_validation->set_message('required', '%s masih kosong, silahkan isi');

		// menampilkan pesan jika nipd sudah tersedia
		$this->form_validation->set_message('is_unique','{field} sudah terdaftar');

		// menampilkan pesan jika password kurang dari 5
		$this->form_validation->set_message('min_length', '{field} minimal  karakter');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('user/register');
		} else {
			$post = $this->input->post(null, TRUE);
			$this->M_user->regis($post);
			if ($this->db->affected_rows() > 0) {
				echo "<script>alert('Data berhasil disimpan');</script>";
			}
			echo "<script>window.location='".base_url('user/register')."';</script>";
		}
	}
}