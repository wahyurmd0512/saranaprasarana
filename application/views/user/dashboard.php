<!DOCTYPE html>
<html class="no-js " lang="en">

<!-- Mirrored from thememakker.com/templates/oreo/html/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Mar 2019 01:59:49 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>Sarana Prasarana</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon"> <!-- Favicon-->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/morrisjs/morris.min.css" />
    <!-- Custom Css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/color_skins.css">
</head>
<body class="theme-purple">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="https://thememakker.com/templates/oreo/html/assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
            <p>Please wait...</p>        
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Top Bar -->
    <nav class="navbar p-l-5 p-r-5">
        <ul class="nav navbar-nav navbar-left">
            <li>
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="index.html"><img src="https://thememakker.com/templates/oreo/html/assets/images/logo.svg" width="30" alt="Oreo"><span class="m-l-10">Sarana Prasarana</span></a>
                </div>
            </li>
            <li class="hidden-sm-down">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                </div>
            </li>        
            <li class="float-right">
                <a href="<?php echo base_url() ?>user/logout" class="mega-menu" data-close="true"><i class="zmdi zmdi-power"></i></a>
                <a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a>
            </li>
        </ul>
    </nav>

    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#dashboard"><i class="zmdi zmdi-home m-r-5"></i>Beranda</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane stretchRight active" id="dashboard">
                <div class="menu">
                    <ul class="list">
                        <li>
                            <div class="user-info">
                                <div class="image"><a href="profile.html"><img src="<?php echo base_url() ?>assets/images/profile_av.jpg" alt="User"></a></div>
                                <div class="detail">
                                    <h4><?= $this->fungsi->user_login()->nama_peminjam ?></h4>
                                    <small>NIPD : <?= $this->fungsi->user_login()->nipd ?></small>
                                    <br>
                                    <small>Peminjam</small>
                                </div>
                            </div>
                        </li>
                        <li class="header">Dashboard</li>
                        <li class="active open"> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-home"></i><span>Menu</span></a>
                            <ul class="ml-menu">
                                <li class="active"><a href="<?= base_url() ?>dashboard/u_dashboard">Beranda</a></li>
                                <li><a href="<?= base_url() ?>barang/inventaris">Inventaris Barang</a> </li>
                                <li><a href="<?= base_url() ?>pinjam/tambah">Pinjam Barang</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>    
    </aside>

    <!-- Right Sidebar -->
    <aside id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane slideRight active" id="setting">
                <div class="slim_scroll">
                    <div class="card">
                        <h6>Skins</h6>
                        <ul class="choose-skin list-unstyled">
                            <li data-theme="purple" class="active"><div class="purple"></div></li>
                            <li data-theme="blue"><div class="blue"></div></li>
                            <li data-theme="cyan"><div class="cyan"></div></li>
                            <li data-theme="green"><div class="green"></div></li>
                            <li data-theme="orange"><div class="orange"></div></li>
                            <li data-theme="blush"><div class="blush"></div></li>
                        </ul>                    
                    </div>
                    <div class="card theme-light-dark">
                        <h6>Left Menu</h6>
                        <button class="t-light btn btn-default btn-simple btn-round btn-block">Light</button>
                        <button class="t-dark btn btn-default btn-round btn-block">Dark</button>
                        <button class="m_img_btn btn btn-primary btn-round btn-block">Sidebar Image</button>
                    </div>
                </div>
            </div>
        </div>
    </aside>

    <!-- Main Content -->
    <section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>Dashboard
                        <small>Selamat datang di Sarana dan Prasarana</small>
                    </h2>
                </div>            
                <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>dashboard/u_dashboard"><i class="zmdi zmdi-home"></i> Beranda</a></li>
                        <li class="breadcrumb-item active">Beranda</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-sm-12 col-md-12">
                    <div class="card">
                        <div class="body">
                            <div class="col-md-12 row">
                                <div class="col-md-3">
                                    <a href="<?= base_url() ?>dashboard/inventarisbarang">
                                        <img src="<?= base_url() ?>assets/images/image1.jpg">
                                        <u style="color: #0000FF;">Inventaris Barang</u>
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <a href="<?= base_url() ?>pinjam/tambah">
                                        <img src="<?= base_url() ?>assets/images/image1.jpg">
                                        <u style="color: #0000FF;">Pinjam Barang</u>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Jquery Core Js --> 
    <script src="<?php echo base_url() ?>assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
    <script src="<?php echo base_url() ?>assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

    <script src="<?php echo base_url() ?>assets/bundles/morrisscripts.bundle.js"></script><!-- Morris Plugin Js -->
    <script src="<?php echo base_url() ?>assets/bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
    <script src="<?php echo base_url() ?>assets/bundles/knob.bundle.js"></script> <!-- Jquery Knob, Count To, Sparkline Js -->

    <script src="<?php echo base_url() ?>assets/bundles/mainscripts.bundle.js"></script>
    <script src="<?php echo base_url() ?>assets/js/pages/index.js"></script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/html/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Mar 2019 02:00:08 GMT -->
</html>