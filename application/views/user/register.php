<!doctype html>
<html class="no-js " lang="en">

<!-- Mirrored from thememakker.com/templates/oreo/html/light/sign-up.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Mar 2019 02:01:18 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

    <title>Sarana Prasarana : Register</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Custom Css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/authentication.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/color_skins.css">
</head>

<body class="theme-purple authentication sidebar-collapse">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-transparent">
        <div class="container">        
            <div class="navbar-translate n_logo">
                <a class="navbar-brand" href="javascript:void(0);" title="" target="_blank">Sarana Prasarana</a>
                <button class="navbar-toggler" type="button">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="navbar-collapse">
                <ul class="navbar-nav">      
                    <li class="nav-item">
                        <a class="nav-link btn btn-white btn-round" href="<?php echo base_url() ?>user/login">Masuk</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="page-header">
        <div class="page-header-image" style="background-image:url(<?= base_url() ?>assets/images/login.jpg)"></div>
        <div class="container">
            <div class="col-md-12 content-center">
                <div class="">
                    <form class="form" method="post" action="<?= base_url() ?>user/register">
                        <div class="header">
                            <div class="logo-container">
                                <img src="https://thememakker.com/templates/oreo/html/assets/images/logo.svg" alt="" style="width: 100px; height: 100px;">
                            </div>
                            <h5>Register Akun</h5>
                            <span>Register akun untuk masuk</span>
                        </div>
                        <br>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <i style="margin-left: 10px;">Nomor Induk Peserta Didik (NIPD) :</i>
                                    </div>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Nomor Induk Peserta Didik" name="nipd" id="nipd" value="<?= set_value('nipd') ?>" />
                                        <span class="input-group-addon">
                                            <i class="zmdi zmdi-border-color"></i>
                                        </span>
                                    </div>
                                    <?= form_error('nipd') ?>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <i style="margin-left: 10px;">Nama Peminjam :</i>
                                    </div>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Nama Pemijam" name="nama_peminjam" id="nama_peminjam" value="<?= set_value('nama_peminjam') ?>" />
                                        <span class="input-group-addon">
                                            <i class="zmdi zmdi-account-circle"></i>
                                        </span>
                                    </div>
                                    <?= form_error('nama_peminjam') ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <i style="margin-left: 10px;">Password :</i>
                                    </div>
                                    <div class="input-group">
                                        <input type="password" placeholder="Password" class="form-control" name="password" id="password" value="<?= set_value('password') ?>" />
                                        <span class="input-group-addon">
                                            <i class="zmdi zmdi-lock"></i>
                                        </span>
                                    </div>
                                    <?= form_error('password') ?>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <i style="margin-left: 10px;">Konfirmasi Password :</i>
                                    </div>
                                    <div class="input-group">
                                        <input type="password" placeholder="Password" class="form-control" name="passconf" id="passconf" value="<?= set_value('passconf') ?>" />
                                        <span class="input-group-addon">
                                            <i class="zmdi zmdi-lock"></i>
                                        </span>
                                    </div>
                                    <?= form_error('passconf') ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <i style="margin-left: 10px;">Telepon :</i>
                                    </div>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Telepon" name="telepon" id="telepon" value="<?= set_value('telepon') ?>" />
                                        <span class="input-group-addon">
                                            <i class="zmdi zmdi-phone"></i>
                                        </span>
                                    </div>
                                    <?= form_error('telepon') ?>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <i style="margin-left: 10px;">Wali Kelas :</i>
                                    </div>
                                     <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Wali Kelas" name="walas" id="walas" value="<?= set_value('walas') ?>" />
                                        <span class="input-group-addon">
                                            <i class="zmdi zmdi-account-circle"></i>
                                        </span>
                                    </div>
                                    <?= form_error('walas') ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <i style="margin-left: 10px;">Status :</i>
                                    </div>
                                    <select name="status" id="status" class="form-control">
                                        <option value=""> -- pilih --</option>
                                        <option value="0" <?= set_value('status') == '0' ? "selected" : null ?>>Tidak Aktif</option>
                                        <option value="1" <?= set_value('status') == '1' ? "selected" : null ?>>Aktif</option>\
                                    </select>
                                    <?= form_error('status') ?>
                                </div>
                                <div class="col-md-6">
                                    <!-- <div class="input-group">
                                        <i style="margin-left: 10px;">Konfirmasi Password :</i>
                                    </div> -->
                                    <div class="input-group">
                                        <input type="hidden" placeholder="Password" class="form-control" name="createtime" id="createtime" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer text-center row">
                            <div class="col-md-6">
                                <button type="reset" class="btn btn-primary btn-round btn-lg btn-block">Reset</button>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-round btn-lg btn-block">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>,
                    <span>Designed by <a href="http://thememakker.com/" target="_blank">SynoDev</a></span>
                </div>
            </div>
        </footer>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url() ?>assets/bundles/libscripts.bundle.js"></script>
    <script src="<?php echo base_url() ?>assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
    <script>
     $(".navbar-toggler").on('click',function() {
        $("html").toggleClass("nav-open");
    });
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/html/light/sign-up.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Mar 2019 02:01:18 GMT -->
</html>