<!DOCTYPE html>
<html class="no-js " lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>Sarana Prasarana</title>
    <link rel="icon" href="<?= base_url() ?>assets/images/logo.png"> <!-- Favicon-->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/morrisjs/morris.min.css" />
    <!-- Custom Css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/color_skins.css">
</head>
<body class="theme-purple">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="https://thememakker.com/templates/oreo/html/assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
            <p>Please wait...</p>        
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Top Bar -->
    <nav class="navbar p-l-5 p-r-5">
        <ul class="nav navbar-nav navbar-left">
            <li>
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href=""><img src="https://thememakker.com/templates/oreo/html/assets/images/logo.svg" width="30" alt="Oreo"><span class="m-l-10">Sarana Prasarana</span></a>
                </div>
            </li>
            <li class="hidden-sm-down">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                </div>
            </li>        
            <li class="float-right">
                <a href="<?php echo base_url() ?>auth/logout" class="mega-menu" data-close="true"><i class="zmdi zmdi-power"></i></a>
                <a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a>
            </li>
        </ul>
    </nav>

    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#dashboard"><i class="zmdi zmdi-home m-r-5"></i>Home</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#user"><i class="zmdi zmdi-account m-r-5"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane stretchRight active" id="dashboard">
                <div class="menu">
                    <ul class="list">
                        <li>
                            <div class="user-info">
                                <div class="image"><a href="profile.html"><img src="<?php echo base_url() ?>assets/images/profile_av.jpg" alt="User"></a></div>
                                <div class="detail">
                                    <h4><?= $this->fungsi->auth_login()->nama_pegawai ?></h4>
                                    <small><?= $this->fungsi->auth_login()->level ?></small>
                                    <br>
                                    <small>NIK : <?= $this->fungsi->auth_login()->nik ?></small>
                                </div>                            
                            </div>
                        </li>
                        <li class="header">Dashboard</li>
                        <li class="active open"> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-home"></i><span>Menu</span></a>
                            <ul class="ml-menu">
                                <li><a href="<?= base_url() ?>dashboard/p_dashboard">Beranda</a></li>
                                <?php if($this->session->userdata('level') === 'Operator') { ?>
                                    <li><a href="">Inventaris Barang</a> </li>
                                    <li><a href="">Pinjam Barang</a></li>
                                    <li><a href="">Pengembalian Barang</a></li>
                                <?php } ?>
                                <?php if($this->session->userdata('level') === 'Admin') { ?>
                                    <li class="active"><a href="<?= base_url() ?>barang/index">Data Barang</a></li>
                                    <li><a href="<?= base_url() ?>barang">Data Peminjaman</a></li>
                                    <li><a href="<?= base_url() ?>barang">Data Pengembalian</a></li>
                                    <li><a href="<?= base_url() ?>auth/v_user">Data Pengguna</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>    
    </aside>

    <!-- Right Sidebar -->
    <aside id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane slideRight active" id="setting">
                <div class="slim_scroll">
                    <div class="card">
                        <h6>Skins</h6>
                        <ul class="choose-skin list-unstyled">
                            <li data-theme="purple" class="active"><div class="purple"></div></li>
                            <li data-theme="blue"><div class="blue"></div></li>
                            <li data-theme="cyan"><div class="cyan"></div></li>
                            <li data-theme="green"><div class="green"></div></li>
                            <li data-theme="orange"><div class="orange"></div></li>
                            <li data-theme="blush"><div class="blush"></div></li>
                        </ul>                    
                    </div>
                    <div class="card theme-light-dark">
                        <h6>Left Menu</h6>
                        <button class="t-light btn btn-default btn-simple btn-round btn-block">Light</button>
                        <button class="t-dark btn btn-default btn-round btn-block">Dark</button>
                        <button class="m_img_btn btn btn-primary btn-round btn-block">Sidebar Image</button>
                    </div>
                </div>
            </div>
        </div>
    </aside>

    <!-- Main Content -->
    <section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>Dashboard
                        <small>Selamat datang di Sarana dan Prasarana</small>
                    </h2>
                </div>            
                <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item"><a href=""><i class="zmdi zmdi-home"></i> Home</a></li>
                        <li class="breadcrumb-item active">Edit Barang</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="body">
                            <div class="header">
                                <h2> <strong>Sarpras</strong> Edit Barang </h2>
                            </div>
                            <div class="body">
                                <form id="form_advanced_validation" method="POST" action="<?= base_url() ?>barang/edit">
                                    <div class="form-group form-float">
                                        <label>Kode Barang</label>
                                        <input type="text" class="form-control" name="kode_barang" id="kode_barang" value="<?= $this->input->post('kode_barang') ?? $data->kode_barang ?>" readonly>
                                    </div>
                                    <div class="form-group form-float">
                                        <label>Nama Barang</label>
                                        <input type="text" class="form-control" name="nama_barang" id="nama_barang" value="<?= $this->input->post('nama_barang') ?? $data->nama_barang ?>">
                                    </div>
                                    <div class="form-group form-float">
                                        <label>Kondisi Barang</label>
                                        <input type="text" class="form-control" name="kondisi_barang" id="kondisi_barang" value="<?= $this->input->post('kondisi_barang') ?? $data->kondisi_barang ?>">
                                    </div>
                                    <div class="form-group form-float">
                                        <label>Keterangan Barang:</label>
                                        <div class="radio inlineblock m-r-20 m-l-20">
                                            <input type="radio" name="keterangan_barang" id="tersedia" class="with-gap" value="Tersedia" <?= set_value('keterangan_barang', $data->keterangan_barang) == 'Tersedia' ? "checked" : ""; ?>>
                                            <label for="tersedia">Tersedia</label>
                                        </div>                                
                                        <div class="radio inlineblock">
                                            <input type="radio" name="keterangan_barang" id="aktif" class="with-gap" value="Dipinjam" <?= set_value('keterangan_barang', $data->keterangan_barang) == 'Dipinjam' ? "checked" : ""; ?>>
                                            <label for="aktif">Dipinjam</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Status : </label>
                                        <div class="radio inlineblock m-r-20 m-l-20">
                                            <input type="radio" name="status" id="tidak_aktif" class="with-gap" value="1" <?= set_value('status', $data->status) == 1 ? "checked" : ""; ?>>
                                            <label for="tidak_aktif">Aktif</label>
                                        </div>                                
                                        <div class="radio inlineblock">
                                            <input type="radio" name="status" id="aktif" class="with-gap" value="0" <?= set_value('status', $data->status) == 0 ? "checked" : ""; ?>>
                                            <label for="aktif">Tidak Aktif</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <input type="hidden" class="form-control" name="createuser" id="createuser">
                                    </div>
                                    <div class="form-group form-float">
                                        <input type="hidden" class="form-control" name="createtime" id="createtime">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-raised btn-info btn-round waves-effect">Simpan</button>
                                        <button type="reset" class="btn btn-raised btn-primary btn-round waves-effect">Reset</button>
                                        <a href="<?= base_url() ?>barang" class="btn btn-raised btn-danger btn-round waves-effect">Kembali</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Jquery Core Js --> 
    <script src="<?php echo base_url() ?>assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
    <script src="<?php echo base_url() ?>assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

    <script src="<?php echo base_url() ?>assets/bundles/morrisscripts.bundle.js"></script><!-- Morris Plugin Js -->
    <script src="<?php echo base_url() ?>assets/bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
    <script src="<?php echo base_url() ?>assets/bundles/knob.bundle.js"></script> <!-- Jquery Knob, Count To, Sparkline Js -->

    <script src="<?php echo base_url() ?>assets/bundles/mainscripts.bundle.js"></script>
    <script src="<?php echo base_url() ?>assets/js/pages/index.js"></script>
</body>
</html>