<?php

// jika belum lgoin tidak bisa akses ke dashboard dan redirect ke form pilih login
function cek_belum_login()
{
	$ci =& get_instance();
	$user_session = $ci->session->userdata('nipd');
	if (!$user_session) {
		redirect(base_url());
	}
}

function belum_login()
{
	$ci =& get_instance();
	$user_session = $ci->session->userdata('nik');
	if (!$user_session) {
		redirect(base_url());
	}
}