<?php

Class Fungsi
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
	}

	function auth_login()
	{
		$this->ci->load->model('M_pegawai');
		$nik = $this->ci->session->userdata('nik');
		$user_data = $this->ci->M_pegawai->get($nik)->row();
		return $user_data;
	}

	function user_login()
	{
		$this->ci->load->model('M_user');
		$nipd = $this->ci->session->userdata('nipd');
		$user_data = $this->ci->M_user->get($nipd)->row();
		return $user_data;
	}
}