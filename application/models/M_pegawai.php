<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class M_pegawai extends CI_Model
{
	// cek akun buat login
	function cekAkunP($post)
	{
		$this->db->select('*');
		$this->db->from('pegawai');
		$this->db->where('nik', $post['nik']);
		$this->db->where('password', sha1($post['password']));
		
		$query = $this->db->get();
		return $query;
	}

	function regis($post)
	{
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');

		$params['nik'] = $post['nik'];
		$params['nama_pegawai'] = $post['nama_pegawai'];
		$params['password'] = sha1($post['password']);
		$params['telepon'] = $post['telepon'];
		$params['level'] = $post['level'];
		$params['status'] = $post['status'];
		$this->db->set('createtime', $now);
		$this->db->insert('pegawai', $params);
	}

	// menampilkan data sesuai tabel di database
	function get($nik = null)
	{
		$this->db->from('pegawai');
		if ($nik != null) {
			$this->db->where('nik', $nik);
		}
		$query = $this->db->get();
		return $query;
	}

}