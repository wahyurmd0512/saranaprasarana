<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class M_user extends CI_Model
{
	
	function cekAkunU($post)
	{
		$this->db->select('*');
		$this->db->from('peminjam');
		$this->db->where('nipd', $post['nipd']);
		$this->db->where('password', sha1($post['password']));

		$query = $this->db->get();
		return $query;
	}

	function regis($post)
	{
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');

		$params['nipd'] = $post['nipd'];
		$params['nama_peminjam'] = $post['nama_peminjam'];
		$params['password'] = sha1($post['password']);
		$params['telepon'] = $post['telepon'];
		$params['walas'] = $post['walas'];
		$params['status'] = $post['status'];
		$this->db->set('createtime', $now);
		$this->db->insert('peminjam', $params);
	}

	// menampilkan data sesuai tabel di database
	function get($nipd = null)
	{
		$this->db->from('peminjam');
		if ($nipd != null) {
			$this->db->where('nipd', $nipd);
		}
		$query = $this->db->get();
		return $query;
	}

	function tampil()
	{
		// menjalankan stored procedure tampil_user()
		$query = $this->db->query("CALL `tampil_user`()");
		mysqli_next_result($this->db->conn_id);
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
	}

	function del($nipd)
	{
		$this->db->where('nipd', $nipd);
		$this->db->delete('peminjam');
	}

}