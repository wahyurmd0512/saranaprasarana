<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class M_barang extends CI_Model
{
	
	function tampil_data($kode_barang = null)
	{	
		$this->db->select('a.*,b.nama_pegawai');
		$this->db->from('barang a');
		if ($kode_barang != null) {
			$this->db->where('kode_barang', $kode_barang);
		}
		$this->db->join('pegawai b','a.createuser = b.nik');
		$query = $this->db->get();
		return $query;
	}

	function add_barang($post)
	{
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');

		$params['kode_barang'] = $post['kode_barang'];
		$params['nama_barang'] = $post['nama_barang'];
		$params['kondisi_barang'] = $post['kondisi_barang'];
		$params['keterangan_barang'] = $post['keterangan_barang'];
		$params['status'] = $post['status'];
		$params['createuser'] = $this->session->userdata('nik');
		$this->db->set('createtime', $now);
		$this->db->insert('barang', $params);
	}

	function edit_barang($post)
	{
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');

		$params['nama_barang'] = $post['nama_barang'];
		$params['kondisi_barang'] = $post['kondisi_barang'];
		$params['keterangan_barang'] = $post['keterangan_barang'];
		$params['status'] = $post['status'];
		$params['createuser'] = $this->session->userdata('nik');
		$this->db->set('createtime', $now);
		$this->db->where('kode_barang',$post['kode_barang']);
		$this->db->update('barang', $params);
	}

	function del($kode_barang)
	{
		$this->db->where('kode_barang', $kode_barang);
		$this->db->delete('barang');
	}

	function export_data()
	{
		return $this->db->get('barang')->result();
	}

	function import_data($filename)
	{
		// me load library upload
		$this->load->library('upload');

		$config['upload_path'] = './excel/';
	    $config['allowed_types'] = 'xlsx';
	    $config['max_size']  = '2048';
	    $config['overwrite'] = true;
	    $config['file_name'] = $filename;
	  
	    $this->upload->initialize($config); // Load konfigurasi uploadnya
	    if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
	      // Jika upload berhasil :
	      $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
	      return $return;
	    }else{
	      // Jika upload gagal :
	      $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
	      return $return;
	    }
	}

	function insert_multiple($data)
	{
		$this->db->insert_batch('barang', $data);
	}

	function get_barang()
	{
		$query = $this->db->get('barang');
		return $query->result();
	}
	
}